CREATE TABLE IF NOT EXISTS developer (
  id              BINARY(16) PRIMARY KEY,
  name            VARCHAR(100) NOT NULL,
  salary           NUMERIC      NOT NULL
)
  ENGINE = InnoDB;