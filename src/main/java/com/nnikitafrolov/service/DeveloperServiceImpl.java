package com.nnikitafrolov.service;

import com.nnikitafrolov.model.Developer;
import com.nnikitafrolov.repository.DeveloperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service("developerService")
@Transactional
public class DeveloperServiceImpl implements DeveloperService{

    @Autowired
    private DeveloperRepository developerRepository;

    @Override
    public Developer findById(UUID id) {
        return developerRepository.findOne(id);
    }

    @Override
    public Developer findByName(String name) {
        return developerRepository.findByName(name);
    }

    @Override
    public void saveDeveloper(Developer developer) {
        developerRepository.save(developer);
    }

    @Override
    public void updateDeveloper(Developer developer) {
        saveDeveloper(developer);
    }

    @Override
    public void deleteDeveloperById(UUID id) {
        developerRepository.delete(id);
    }

    @Override
    public void deleteAllDevelopers() {
        developerRepository.deleteAll();
    }

    @Override
    public List<Developer> findAllDevelopers() {
        return developerRepository.findAll();
    }

    @Override
    public boolean isDeveloperExist(Developer developer) {
        return findByName(developer.getName()) != null;
    }
}
