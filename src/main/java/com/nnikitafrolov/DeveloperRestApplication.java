package com.nnikitafrolov;

import com.nnikitafrolov.configuration.JpaConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages = {"com.nnikitafrolov"})
public class DeveloperRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeveloperRestApplication.class, args);
	}
}
