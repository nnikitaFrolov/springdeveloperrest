package com.nnikitafrolov.rest;

import com.nnikitafrolov.model.Developer;
import com.nnikitafrolov.service.DeveloperService;
import com.nnikitafrolov.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("/api")
public class RestApiController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

    @Autowired
    DeveloperService developerService;

    // -------------------Retrieve All Developers---------------------------------------------

    @RequestMapping(value = "/developer/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Developer>> listAllDevelopers() {
        List<Developer> developers = developerService.findAllDevelopers();
        if (developers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(developers, HttpStatus.OK);
    }

    // -------------------Retrieve Single Developer------------------------------------------

    @RequestMapping(value = "/developer/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDeveloper(@PathVariable("id") UUID id) {
        logger.info("Fetching Developer with id {}", id);
        Developer developer = developerService.findById(id);
        if (developer == null) {
            logger.error("Developer with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Developer with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(developer, HttpStatus.OK);
    }

    // -------------------Create a Developer-------------------------------------------

    @RequestMapping(value = "/developer/", method = RequestMethod.POST)
    public ResponseEntity<?> createDeveloper(@RequestBody Developer developer, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Developer : {}", developer);

        if (developerService.isDeveloperExist(developer)) {
            logger.error("Unable to create. A Developer with id {} already exist", developer.getId());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A Developer with id " +
                    developer.getId() + " already exist."), HttpStatus.CONFLICT);
        }
        developerService.saveDeveloper(developer);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/developer/{id}").buildAndExpand(developer.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    // ------------------- Update a Developer ------------------------------------------------

    @RequestMapping(value = "/developer/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateDeveloper(@PathVariable("id") UUID id, @RequestBody Developer developer) {
        logger.info("Updating Developer with id {}", id);

        Developer currentDeveloper = developerService.findById(id);

        if (currentDeveloper == null) {
            logger.error("Unable to update. Developer with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Developer with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentDeveloper.setName(developer.getName());
        currentDeveloper.setSalary(developer.getSalary());

        developerService.updateDeveloper(currentDeveloper);
        return new ResponseEntity<>(currentDeveloper, HttpStatus.OK);
    }

    // ------------------- Delete a Developer-----------------------------------------

    @RequestMapping(value = "/developer/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDeveloper(@PathVariable("id") UUID id) {
        logger.info("Fetching & Deleting Developer with id {}", id);

        Developer developer = developerService.findById(id);
        if (developer == null) {
            logger.error("Unable to delete. Developer with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Developer with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        developerService.deleteDeveloperById(developer.getId());
        return new ResponseEntity<Developer>(HttpStatus.NO_CONTENT);
    }

    // ------------------- Delete All Developers-----------------------------

    @RequestMapping(value = "/developer/", method = RequestMethod.DELETE)
    public ResponseEntity<Developer> deleteAllDevelopers() {
        logger.info("Deleting All Developers");

        developerService.deleteAllDevelopers();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
