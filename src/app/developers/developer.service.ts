import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Developer} from "./developer";
import {Observable} from "rxjs";
import {UUID} from "angular2-uuid";

@Injectable()
export class DeveloperService {
  private entity_url = environment.REST_API_URL + 'developer';

  constructor(private _http: Http) {
  }

  getDevelopers():Observable<Developer[]>{
    return this._http.get(this.entity_url + '/')
      .map((response: Response) => <Developer[]> response.json())
      .catch((error:any) =>{return Observable.throw(error);});
  }

  getDeveloperById(developer_id: UUID):Observable<Developer>{
    return this._http.get(this.entity_url + '/' + developer_id)
      .map((response: Response) => <Developer> response.json())
      .catch((error:any) =>{return Observable.throw(error);});

  }

  addDeveloper(developer: Developer):Observable<Developer>{
    const headers = new Headers();
    const body = JSON.stringify(developer);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this._http.post(this.entity_url + '/', body, {headers})
      .map((response: Response) => <Developer> response.json())
      .catch((error:any) =>{return Observable.throw(error);});
  }

  updateDeveloper(developer_id: UUID, developer: Developer):Observable<Developer>{
    const body = JSON.stringify(developer);
    const headers = new Headers({'Content-Type': ' application/json;charset=UTF-8'});
    const options = new RequestOptions({headers: headers});
    console.log(body);
    return this._http.put((this.entity_url + '/' + developer_id), body, options)
      .map((response: Response) => response)
      .catch((error:any) =>{return Observable.throw(error);});
  }

  deleteDeveloper(developer_id: UUID): Observable<number> {
    const headers = new Headers({'Content-Type': ' application/json;charset=UTF-8'});
    const options = new RequestOptions({headers: headers});
    return this._http.delete(this.entity_url + '/' + developer_id, options)
      .map((response: Response) => response.status)
      .catch((error:any) =>{return Observable.throw(error);});
  }
}
