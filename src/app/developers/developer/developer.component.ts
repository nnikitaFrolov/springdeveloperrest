import {Component, Input, OnInit} from '@angular/core';
import {Developer} from "../developer";
import {Router} from "@angular/router";
import {DeveloperService} from "../developer.service";

@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.css']
})
export class DeveloperComponent implements OnInit {

  @Input()developer:Developer;
  errorMessage: string;
  delete_success: boolean = false;

  constructor(private router: Router, private developerService: DeveloperService) {
    this.developer = <Developer>{};
  }

  ngOnInit() {
  }

  editDeveloper(developer: Developer) {
    this.router.navigate(['/developer', developer.id, 'edit']);
  }

  deleteDeveloper(developer: Developer) {
    this.developerService.deleteDeveloper(developer.id).subscribe(
      response => {
        if (response === 204) {
          this.delete_success = true;
          this.developer = <Developer>{};
        }
      },
      error => this.errorMessage = <any> error);
  }

}
