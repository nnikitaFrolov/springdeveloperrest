import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {DeveloperService} from "../developer.service";
import {Developer} from "../developer";

@Component({
  selector: 'app-developer-add',
  templateUrl: './developer-add.component.html',
  styleUrls: ['./developer-add.component.css']
})
export class DeveloperAddComponent implements OnInit {
  errorMessage: string;
  developer: Developer;

  constructor(private developerService: DeveloperService, private router: Router) {
    this.developer = <Developer>{};
  }

  ngOnInit() {
  }

  onSubmit(developer: Developer) {
    developer.id = null;
    this.developerService.addDeveloper(developer).subscribe(
      respone => {
        this.developer = developer;
      },
      error => this.errorMessage = <any> error
    );
  }

}
