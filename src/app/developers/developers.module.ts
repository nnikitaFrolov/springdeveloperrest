
import {NgModule} from '@angular/core';
import {DeveloperService} from "./developer.service";
import {FormsModule} from "@angular/forms";
import { DeveloperListComponent } from './developer-list/developer-list.component';
import { DeveloperEditComponent } from './developer-edit/developer-edit.component';
import { DeveloperAddComponent } from './developer-add/developer-add.component';
import { DeveloperComponent } from './developer/developer.component';
import {DeveloperRoutingModule} from "./developers-routing.module";
import {CommonModule} from "@angular/common";

@NgModule({
  imports:[
    CommonModule,
    FormsModule,
    DeveloperRoutingModule
  ],
  declarations:[
    DeveloperListComponent,
    DeveloperEditComponent,
    DeveloperAddComponent,
    DeveloperComponent],
  exports:[
    DeveloperListComponent,
    DeveloperEditComponent,
    DeveloperAddComponent,
    DeveloperComponent
  ],
  providers:[DeveloperService]
})
export class DevelopersModule{
}
