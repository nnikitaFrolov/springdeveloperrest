import {Component, OnInit} from '@angular/core';
import {Developer} from "../developer";
import {DeveloperService} from "../developer.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-developer-list',
  templateUrl: './developer-list.component.html',
  styleUrls: ['./developer-list.component.css']
})
export class DeveloperListComponent implements OnInit {
  developers: Developer[];
  errorMessage: string;

  constructor(private developerService: DeveloperService, private router: Router) {
    this.developers = [];
  }

  ngOnInit() {
    this.developerService.getDevelopers().subscribe(
      developers => this.developers = developers,
      error => this.errorMessage = <any> error);
  }

  addDeveloper() {
    this.router.navigate(['/developer','add']);
  }
}
