# REST version of SpringDeveloper Sample Application

This backend version of the SpringDeveloper application only provides a REST API. There is no UI.
The [developer-angular project](https://bitbucket.org/nnikitaFrolov/springdeveloperangular/overview) is a front-end application witch consumes the REST API.

## Running petclinic locally
```
	git clone https://nnikitaFrolov@bitbucket.org/nnikitaFrolov/springdeveloperrest.git
	cd developer-rest
	./mvnw spring-boot:run
```

You can then access petclinic here: http://localhost:8080/DeveloperRest/api/